<?php

namespace FlowControl\Filters\Types;

use FlowControl\Form\Field\AbstractType;
use FlowControl\Form\Field\Time as TimeField;

class Time extends Date
{
    /**
     * @return AbstractType
     */
    public function formField()
    {
        return new TimeField($this->field(), $this->label(), $this->options());
    }
}