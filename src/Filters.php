<?php

namespace FlowControl\Filters;

use FlowControl\Form\Field\Group;
use FlowControl\Form\Field\Submit;
use FlowControl\Form\FormBuilder;
use FlowControl\Filters\Types\Type;

/**
 * @method Filters text($name, $label, $options = null)
 * @method Filters email($name, $label, $options = null)
 * @method Filters select($name, $label, $options = null)
 * @method Filters date($name, $label, $options = null)
 * @method Filters time($name, $label, $options = null)
 * @method Filters datetime($name, $label, $options = null)
 * @method Filters dateBetween($name, $label, $options = null)
 * @method Filters timeBetween($name, $label, $options = null)
 * @method Filters boolean($name, $label, $options = null)
 */
class Filters
{
    protected $filters = [];

    public function getData()
    {
        $filters = [];

        foreach($this->filters as $field => $filter)
        {
            $filters[$field] = $filter->value();
        }

        return $filters;
    }

    /**
     * @return string
     */
    public function render()
    {
        if (count($this->filters) === 0) {
            return null;
        }

        $page = request()->has('page') ? '?page=' . request('page') : null;
        $clearUrl = url()->current() . $page;

        if (! $filterBtn = config('flowcontrol.filters.filterBtn') instanceof Submit) {
            $filterBtn = new Submit(
                config('flowcontrol.filters.filterBtn.name'),
                config('flowcontrol.filters.filterBtn.label'),
                config('flowcontrol.filters.filterBtn.options')
            );
        }

        $clearBtn = [
            'label' => config('flowcontrol.filters.clearBtn.label'),
            'url' => $clearUrl,
        ];

        return (new Group('', '', function(FormBuilder $builder) {
            foreach($this->filters as $filter) {
                $builder->add($filter->formField());
            }
        }))
            ->setView('flowcontrol/filters::filters')
            ->render([], [
                'filterBtn' => $filterBtn,
                'clearBtn' => $clearBtn,
                'clearUrl' => $clearUrl,
            ]);
    }

    /**
     * @param Type $filter
     * @return $this
     */
    public function add(Type $filter)
    {
        $this->filters[$filter->field()] = $filter;
        return $this;
    }

    public function __call($name, $args = [])
    {
        $class = '\FlowControl\Filters\Types\\' . studly_case($name);

        if(!class_exists($class)) {
            $class = '\FlowControl\Filters\Types\Text';
        }

        if(count($args) === 2)
        {
            $args[] = [];
        }

        return $this->add(app($class, $args));
    }
}