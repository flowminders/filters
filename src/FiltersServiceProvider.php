<?php

namespace FlowControl\Filters;

use Illuminate\Support\ServiceProvider;

class FiltersServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/Views', 'flowcontrol/filters');
        $this->publishes([
            __DIR__ . '/Views' => resource_path('views/vendor/flowcontrol/filters')
        ], 'views');

        $this->publishes([
            __DIR__ . '/Config/flowcontrol.filters.php' => config_path('flowcontrol.filters.php')
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/Config/flowcontrol.filters.php', 'flowcontrol.filters');
    }
}