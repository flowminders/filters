<?php

namespace FlowControl\Filters\Types;

use FlowControl\Form\Field\Group;
use FlowControl\Form\FormBuilder;

class TimeBetween extends DateBetween
{
    public function formField()
    {
        return (new Group($this->field(), $this->label(), function(FormBuilder $builder) {
            $builder
                ->time("{$this->field()}_start", '', ['placeholder' => 'Начален час'])
                ->time("{$this->field()}_end", '', ['placeholder' => 'Краен час']);
        }))
            ->setView('flowcontrol/filters::between');
    }
}